const url = "https://ajax.test-danit.com/api/json/";

const root = document.querySelector(".root");

function sendRequest(url) {
  return fetch(url);
}

function displayPost() {
  Promise.all([sendRequest(`${url}users`), sendRequest(`${url}posts`)])

    .then((responses) =>
      Promise.all(responses.map((response) => response.json()))
    )

    .then((data) => {
      const [users, posts] = data;

      posts.forEach((post) => {
        const user = users.find((user) => user.id === post.userId);
        const card = new Card(post, user);
        root.append(card.cardElement);
      });
    })
    .catch((error) => console.error(error));
}

displayPost();


class Card {
  constructor(post, user) {
    (this.post = post),
      (this.user = user),
      (this.cardElement = this.createCard());
  }
  createCard() {
    const box = document.createElement("div");
    box.className = "box-post";
    root.prepend(box);

    const header = document.createElement("div");
    header.className = "post-header";
    box.append(header);

    const name = document.createElement("h2");
    name.innerHTML = `${this.user.name} ${this.user.username}`;
    header.append(name);

    const email = document.createElement("a");
    email.href = `mailto:${this.user.email}`;
    email.innerHTML = `${this.user.email}`;
    header.append(email);

    const title = document.createElement("h3");
    title.innerHTML = `${this.post.title}`;
    box.append(title);

    const body = document.createElement("p");
    body.innerHTML = `${this.post.body}`;
    box.append(body);

    const button = document.createElement("button");
    button.innerText = "DELETE";
    button.addEventListener("click", () => {
      this.deleteCard();
    });

    box.append(button);

    return box;
  }

  deleteCard() {
    fetch(`${url}posts/${this.post.id}`, {
      method: "DELETE",
    })
      .then((response) => {
        if (response.ok) {
          this.cardElement.remove();
          console.log(
            `Post number ${this.post.id} made by ${this.user.name} with title "${this.post.title}" deleted successfully`
          );
        } else {
          throw new Error("Unable to delete card");
        }
      })
      .catch((error) => console.error(error));
  }
}
